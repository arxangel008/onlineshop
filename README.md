#onlineShop

Stages of deployment of the project:
1) clone a repository
2) open to Intelij Idea as maven project

requests:
1) GET: http://localhost:8080/onlineShop/items  - outputs the list of all items
2) GET: http://localhost:8080/onlineShop/items/1 - outputs information on one item
   response:
   {
    "id": 1,
    "brand": "INDESIT",
    "title": "BI 160",
    "price": 13999,
    "count": 15,
    "groupType": {
        "id": 1,
        "title": "Холодильник"
    },
    "value": [
        {
            "id": 2,
            "value": "299 л",
            "type": {
                "id": 2,
                "title": "Объем холодильной камеры"
            }
        },
        {
            "id": 1,
            "value": "Расположение - Нижнее; Система размораживания - Ручная",
            "type": {
                "id": 1,
                "title": "Морозильная камера"
            }
        }
    ]
  }
3) PUT: http://localhost:8080/onlineShop/items/1?count=13&price=15490 - changes the price and quantity of commodity items
   response:
   {
    "id": 1,
    "brand": "INDESIT",
    "title": "BI 160",
    "price": 15490,
    "count": 13,
    "groupType": {
        "id": 1,
        "title": "Холодильник"
    },
    "value": [
        {
            "id": 2,
            "value": "299 л",
            "type": {
                "id": 2,
                "title": "Объем холодильной камеры"
            }
        },
        {
            "id": 1,
            "value": "Расположение - Нижнее; Система размораживания - Ручная",
            "type": {
                "id": 1,
                "title": "Морозильная камера"
            }
        }
    ]
  }
4) GET: http://localhost:8080/onlineShop/items/1/value - outputs values of characteristics of items
   response:
   [
    {
        "id": 2,
        "value": "299 л",
        "type": {
            "id": 2,
            "title": "Объем холодильной камеры"
        }
    },
    {
        "id": 1,
        "value": "Расположение - Нижнее; Система размораживания - Ручная",
        "type": {
            "id": 1,
            "title": "Морозильная камера"
        }
    }
  ]
5) GET: http://localhost:8080/onlineShop/items/1/value/1 - outputs concrete value of items
    response:
    {
    "id": 1,
    "value": "Расположение - Нижнее; Система размораживания - Ручная",
    "type": {
        "id": 1,
        "title": "Морозильная камера"
    }
    }
6) PUT http://localhost:8080/onlineShop/items/1/value/1?value=Система размораживания - No Frost - change concrete value of items
  response:
    {
    "id": 1,
    "value": "Система размораживания - No Frost",
    "type": {
        "id": 1,
        "title": "Морозильная камера"
    }
    }
