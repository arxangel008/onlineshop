package com.onlineShop.controller;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.onlineShop.model.GroupType;
import com.onlineShop.model.Item;
import com.onlineShop.model.Type;
import com.onlineShop.model.Value;
import com.onlineShop.repository.ItemRepository;
import com.onlineShop.repository.ValueRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@RestController
@RequestMapping("/items")
public class ItemController implements ErrorController{

    @Autowired
    private ItemRepository itemRepository;

    @Autowired
    private ValueRepository valueRepository;

    private static final String PATH = "/error";

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Collection<Item>> getItems() {
        return new ResponseEntity<>(itemRepository.findAll(), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Item> getItem(@PathVariable long id) {
        Item item = itemRepository.findOne(id);

        if (item != null) {
            return new ResponseEntity<>(item, HttpStatus.OK);
        } else {
            return new ResponseEntity<>((Item) null, HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{id}/value", method = RequestMethod.GET)
    public ResponseEntity<Collection<Value>> getValuesByItemId(@PathVariable long id) {
        Item item = itemRepository.findOne(id);

        if (item != null) {
            return new ResponseEntity<>(item.getValue(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>((Collection<Value>) null, HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{id}/value/{valueId}", method = RequestMethod.GET)
    public ResponseEntity<Value> getValue(@PathVariable long id, @PathVariable long valueId) {
        Value value = valueRepository.findOne(valueId);

        Collection<Value> colVal = itemRepository.findOne(id).getValue();

        if (colVal.contains(value)) {
            return new ResponseEntity<>(value, HttpStatus.OK);
        } else {
            return new ResponseEntity<>((Value) null, HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{id}/value/{valueId}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateValue(@PathVariable long id, @PathVariable long valueId, @RequestParam("value") String v) {
        Value value = valueRepository.findOne(valueId);

        Collection<Value> colVal = itemRepository.findOne(id).getValue();

        if (colVal.contains(value)) {
            value.setValue(v);
            return new ResponseEntity<>(valueRepository.save(value), HttpStatus.OK);
        } else {
            return new ResponseEntity<>((Value) null, HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateItem(@PathVariable("id") long id, @RequestParam("price") Integer price, @RequestParam("count") Integer count) {
        Item item = itemRepository.findOne(id);

        if (item != null) {
            if(price != null){
                item.setPrice(price);
            }
            if (count != null){
                item.setCount(count);
            }
            return new ResponseEntity<>(itemRepository.save(item), HttpStatus.OK);
        } else {
            return new ResponseEntity<>((Item) null, HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteItem(@PathVariable long id) {
        itemRepository.delete(id);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @RequestMapping(value = PATH)
    public String error() {
        return "Error handling";
    }

    @Override
    public String getErrorPath() {
        return PATH;
    }

}
