package com.onlineShop.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;

@Entity
@Table(name = "item_values")
public class Value {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "item_values_id")
    private long id;

    private String value;

    @ManyToOne
    @JoinColumn(name = "items_id")
    @JsonBackReference
    private Item item;

    @ManyToOne
    @JoinColumn(name = "items_types_id")
    private Type type;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }
}
