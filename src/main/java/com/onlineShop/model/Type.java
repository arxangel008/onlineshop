package com.onlineShop.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "items_types")
public class Type {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "items_types_id")
    private long id;

    private String title;

    @ManyToOne
    @JoinColumn (name="group_types_id")
    @JsonBackReference
    private GroupType groupType;

    @OneToMany(cascade=CascadeType.ALL, mappedBy = "type", fetch = FetchType.LAZY)
    @JsonBackReference
    private Set<Value> value = new HashSet<Value>();

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public GroupType getGroupType() {
        return groupType;
    }

    public void setGroupType(GroupType groupType) {
        this.groupType = groupType;
    }

    public Set<Value> getValue() {
        return value;
    }

    public void setValue(Set<Value> value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Type{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", groupType=" + groupType +
                ", value=" + value +
                '}';
    }
}
