package com.onlineShop.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "group_types")
public class GroupType implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "group_types_id")
    private long id;

    private String title;

    @OneToMany(mappedBy = "groupType")
    @JsonBackReference
    private Set<Item> item = new HashSet<Item>();

    @OneToMany(mappedBy = "groupType")
    @JsonBackReference
    private Set<Type> type = new HashSet<Type>();

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Set<Item> getItem() {
        return item;
    }

    public void setItem(Set<Item> item) {
        this.item = item;
    }

    public Set<Type> getType() {
        return type;
    }

    public void setType(Set<Type> type) {
        this.type = type;
    }
}
