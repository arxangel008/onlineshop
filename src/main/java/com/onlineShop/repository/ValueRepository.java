package com.onlineShop.repository;

import com.onlineShop.model.Value;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface ValueRepository extends CrudRepository<Value, Long>{

    Collection<Value> findAll();
}
