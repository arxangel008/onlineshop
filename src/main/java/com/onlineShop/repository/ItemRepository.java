package com.onlineShop.repository;

import com.onlineShop.model.Item;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface ItemRepository extends CrudRepository<Item, Long> {

    Collection<Item> findAll();
}
