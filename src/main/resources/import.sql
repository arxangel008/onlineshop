INSERT INTO group_types (group_types_id, title) VALUES (1, 'Холодильник');
INSERT INTO group_types (group_types_id, title) VALUES (2, 'Телевизор');
INSERT INTO group_types (group_types_id, title) VALUES (3, 'СтиральнаяМашина');

INSERT INTO items_types (items_types_id, group_types_id, title) VALUES (1, 1, 'Морозильная камера');
INSERT INTO items_types (items_types_id, group_types_id, title) VALUES (2, 1, 'Объем холодильной камеры');
INSERT INTO items_types (items_types_id, group_types_id, title) VALUES (3, 2, 'Диагональ');
INSERT INTO items_types (items_types_id, group_types_id, title) VALUES (4, 2, 'Поддержка 3D');
INSERT INTO items_types (items_types_id, group_types_id, title) VALUES (5, 2, 'Технология');
INSERT INTO items_types (items_types_id, group_types_id, title) VALUES (6, 3, 'Тип загрузки');
INSERT INTO items_types (items_types_id, group_types_id, title) VALUES (7, 3, 'Глубина');
INSERT INTO items_types (items_types_id, group_types_id, title) VALUES (8, 3, 'Ширина');
INSERT INTO items_types (items_types_id, group_types_id, title) VALUES (9, 3, 'Высота');

INSERT INTO items (items_id, brand, title, price, count, group_types_id) VALUES (1, 'INDESIT', 'BI 160', 13999, 15, 1);
INSERT INTO items (items_id, brand, title, price, count, group_types_id) VALUES (2, 'LG', 'GA-E409SQRL', 32999, 3, 1);
INSERT INTO items (items_id, brand, title, price, count, group_types_id) VALUES (3, 'SAMSUNG', 'UE48HU8500T', 69290, 1, 2);
INSERT INTO items (items_id, brand, title, price, count, group_types_id) VALUES (4, 'LG', '43UH610V', 32990, 7, 2);
INSERT INTO items (items_id, brand, title, price, count, group_types_id) VALUES (5, 'INDESIT', 'EWUC 4105', 10999, 25, 3);
INSERT INTO items (items_id, brand, title, price, count, group_types_id) VALUES (6, 'LG', 'E10B8ND', 21989, 6, 3);

INSERT INTO item_values (item_values_id, items_types_id, items_id, value) VALUES (1, 1, 1, 'Расположение - Нижнее; Система размораживания - Ручная');
INSERT INTO item_values (item_values_id, items_types_id, items_id, value) VALUES (2, 2, 1, '299 л');
INSERT INTO item_values (item_values_id, items_types_id, items_id, value) VALUES (3, 1, 2, 'Расположение - Нижнее; Система размораживания - No Frost');
INSERT INTO item_values (item_values_id, items_types_id, items_id, value) VALUES (4, 2, 2, '354 л');
INSERT INTO item_values (item_values_id, items_types_id, items_id, value) VALUES (5, 3, 3, '48" (121.9 см)');
INSERT INTO item_values (item_values_id, items_types_id, items_id, value) VALUES (6, 4, 3, 'Активное');
INSERT INTO item_values (item_values_id, items_types_id, items_id, value) VALUES (7, 5, 3, 'LED');
INSERT INTO item_values (item_values_id, items_types_id, items_id, value) VALUES (8, 3, 4, '43" (109.2 см)');
INSERT INTO item_values (item_values_id, items_types_id, items_id, value) VALUES (9, 4, 4, 'Нет');
INSERT INTO item_values (item_values_id, items_types_id, items_id, value) VALUES (10, 5, 4, 'LED');