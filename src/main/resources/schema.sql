CREATE TABLE group_types (
  group_types_id BIGINT PRIMARY KEY AUTO_INCREMENT,
  title          VARCHAR(32),
);

CREATE TABLE items_types (
  items_types_id BIGINT PRIMARY KEY AUTO_INCREMENT,
  group_types_id BIGINT REFERENCES group_types (group_types_id),
  title          VARCHAR(32),
);

CREATE TABLE items (
  items_id        BIGINT PRIMARY KEY AUTO_INCREMENT,
  brand          VARCHAR(32),
  title          VARCHAR(32),
  price          INT,
  count          INT,
  group_types_id BIGINT REFERENCES group_types (group_types_id)
);

CREATE TABLE item_values (
  item_values_id BIGINT PRIMARY KEY AUTO_INCREMENT,
  items_types_id       BIGINT REFERENCES items_types (items_types_id),
  items_id        BIGINT REFERENCES items (items_id),
  value          VARCHAR(256),
);