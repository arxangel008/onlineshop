package com.onlineShop;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.onlineShop.controller.ItemController;
import com.onlineShop.model.GroupType;
import com.onlineShop.model.Item;
import com.onlineShop.model.Type;
import com.onlineShop.model.Value;
import com.onlineShop.repository.ItemRepository;
import com.onlineShop.repository.ValueRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.charset.Charset;
import java.util.HashSet;
import java.util.Set;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@WebMvcTest(ItemController.class)
public class OnlineShopApplicationTests {

    @Autowired
    private MockMvc mvc;

    private MediaType contentType = new MediaType(          MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));

    @MockBean
    private ItemRepository itemRepository;

    @MockBean
    private ValueRepository valueRepository;

    private Item item = new Item();
    private GroupType groupType = new GroupType();
    private Type additiveType = new Type();
    private Value additiveValue = new Value();
    private Set<Value> value = new HashSet<Value>();

    @Test
    public void getItem() throws Exception {

        additiveType.setId(1);
        additiveType.setTitle("Морозильная камера");
        additiveValue.setId(1);
        additiveValue.setValue("Расположение - Нижнее; Система размораживания - Ручная");
        additiveValue.setType(additiveType);
        value.add(additiveValue);
        additiveType = new Type();
        additiveValue = new Value();
        additiveType.setId(2);
        additiveType.setTitle("Объем холодильной камеры");
        additiveValue.setId(2);
        additiveValue.setValue("299 л");
        additiveValue.setType(additiveType);
        value.add(additiveValue);

        groupType.setId(1);
        groupType.setTitle("Холодильник");

        item.setId(1);
        item.setBrand("INDESIT");
        item.setTitle("BI 160");
        item.setCount(15);
        item.setPrice(13999);
        item.setGroupType(groupType);
        item.setValue(value);

        when(itemRepository.findOne(1L)).thenReturn(item);
        mvc.perform(get("/items/1")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json("{\n" +
                        "    \"id\": 1,\n" +
                        "    \"brand\": \"INDESIT\",\n" +
                        "    \"title\": \"BI 160\",\n" +
                        "    \"price\": 13999,\n" +
                        "    \"count\": 15,\n" +
                        "    \"groupType\": {\n" +
                        "        \"id\": 1,\n" +
                        "        \"title\": \"Холодильник\"\n" +
                        "    },\n" +
                        "    \"value\": [\n" +
                        "        {\n" +
                        "            \"id\": 1,\n" +
                        "            \"value\": \"Расположение - Нижнее; Система размораживания - Ручная\",\n" +
                        "            \"type\": {\n" +
                        "                \"id\": 1,\n" +
                        "                \"title\": \"Морозильная камера\"\n" +
                        "            }\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"id\": 2,\n" +
                        "            \"value\": \"299 л\",\n" +
                        "            \"type\": {\n" +
                        "                \"id\": 2,\n" +
                        "                \"title\": \"Объем холодильной камеры\"\n" +
                        "            }\n" +
                        "        }\n" +
                        "    ]\n" +
                        "}"));
    }

}
